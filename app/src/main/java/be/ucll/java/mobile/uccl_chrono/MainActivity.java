package be.ucll.java.mobile.uccl_chrono;


import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Chronometer;

public class MainActivity extends AppCompatActivity {
 private Chronometer chronometer;
 private boolean running;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chronometer = findViewById(R.id.chronometer);
    }
    public void Start(View v) {
     if(!running) {
         chronometer.start();
     }
    }
    public void Stop(View v) {
        if(running) {
            chronometer.stop();
            running =false;
        }


    }
}